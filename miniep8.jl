using Test

# auxiliar functions

function isLetter(value)
    if value == "A" || value == "K" || value == "J" || value == "Q"
        return true
    end
    return false
end

function findIndexInArr(element, ar)
    for index = 1:length(ar)
        item = ar[index]
        if element == item
            return index
        end
    end
    return -1
end

# parte1

function compareByValue(x, y)
    letterOrder = ["J","Q","K","A"]

    # getting all but the last character as a string
    xValue = x[1:end - 1]
    yValue = y[1:end - 1]

    if isLetter(xValue) && isLetter(yValue)
        return findIndexInArr(xValue, letterOrder) < findIndexInArr(yValue, letterOrder)
    end
    if isLetter(xValue) && !isLetter(yValue)
        return false
    end
    if !isLetter(xValue) && isLetter(yValue)
        return true
    end
    # xValue and yValue are numbers
    return parse(Int64, xValue) < parse(Int64, yValue)
end

# parte 2
function compareByValueAndSuit(x, y)
    suitsOrder = ["♦", "♠", "♥", "♣"]
    # getting the last character (card suit) as a string
    xSuit = x[end:end]
    ySuit = y[end:end]

    if xSuit == ySuit
        return compareByValue(x, y)
    end
    return findIndexInArr(xSuit, suitsOrder) < findIndexInArr(ySuit, suitsOrder)
        

end


# problem functions

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end


# testing

function test()
    # pt.1 tests
    @test (compareByValue("2♠", "A♠") == true)
    @test (compareByValue("K♥", "10♥") == false)
    @test (compareByValue("10♠", "10♥") == false)

    # pt.2 tests
    @test compareByValueAndSuit("2♠", "A♠") == true
    @test compareByValueAndSuit("K♥", "10♥") == false
    @test compareByValueAndSuit("10♠", "10♥") == true 
    @test compareByValueAndSuit("A♠", "2♥") == true

    # problem tests
    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
    @test insercao(["10♥", "K♦", "K♠", "2♠", "J♥", "A♠"]) == ["K♦", "2♠", "K♠", "A♠", "10♥", "J♥"]
end

test()